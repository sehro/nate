#!/usr/bin/env python2
##
## nate.py: Please see the README.md for more information
##
import numpy
import signal
import sys

# Graceful Signal handling
def signal_handler(signal, frame):
    print "\n\nOh, okay, I'll stop now. Sorry about that."
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

# Say hello
stHO = "This script will compute the products of up to 10 single or double digit "
stHO += "integers in an array,\ncycling through each while ignoring the "
stHO += "current index. \n"
print stHO

# Error String Definitions
erIB = "Invalid Bounds: Please retry with a minimum of 3 and a maximum of 10"
erLI = "Invalid Integer: Amusing as it is, 0 will just bork the whole thing."
erHI = "Invalid Integer: As much fun as large numbers are, they are not supported."
erNI = "Invalid Integer: Literally not a number. Try again!"

# The super array (where the magick happens)
valx = []

# Function that does the thing with the magick
def get_products_of_all_ints_except_at_index(warp):
    # Generate a numpy array to use numpy functions on it. Stupid numpy.
    drive = numpy.array(warp)

    # Create a hole to shove things out
    valy = []

    # Iterate over the (stupid) numpy array
    for i, elem in enumerate(warp):
        # For each index, make the array not include the value in that index
        speed = drive[drive!=warp[i]]
        # Then shove the product of all remaining elements of said array
        # into the out-hole as a new array value
        valy.append(numpy.prod(speed))
    # Dump the outhole (with leading blank line to make it pretty)
    print "\nFinal tally: " + str(valy)

# Function that gets the things what with to do the magick
def get_integer(incstep):
    isSafe = 0
    while isSafe == 0:
        incoming = raw_input("Enter number #" + str(incstep) + ": ")
        isSafe = 1
        try:
            outgoing = int(incoming)
            if outgoing < 1:
                print erLI
                isSafe = 0
            if outgoing > 99:
                print erHI
                isSafe = 0
        except:
            print erNI
            isSafe = 0
    return outgoing


# essentially main
def modern_magick():
    # Function global variables
    valx = []
    isAllGood = 0
    stCurrentError = "Undefined"

    # Get Bounds
    valcnti = raw_input("How many numbers would you like to compute? ")
    try:
        valcnt = int(valcnti)
    except:
        print "\nError! That's not a number. Try again later."
        sys.exit(0)

    # Die if the count is too boring or not boring enough
    if valcnt < 3 or valcnt > 10:
        isAllGood = 1
    else:
        #Get individual integers
        for valstep in range (1,valcnt+1):
            valitem = get_integer(valstep)
            valx.append(valitem)

    if isAllGood > 0:
        print "\n" + erIB
    else:
        get_products_of_all_ints_except_at_index(valx)

# Time to do the thing!
modern_magick()
