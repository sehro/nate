# nate.py
> I'm so sorry
-sehro

## Origin and description

Originally called netapp-test-ext: I wanted to complete it so I did. Then I wanted it to go beyond what was absolutely necessary, so I could (on demand) demonstrate actual grasp of the principles involved. I stayed up until 4 am making this steaming pile, which contains no real naming convention besides "that sounds cool." I've released this to the public for several reasons (raisins?):

1. Iunno, it's actually a good demonstration of a particular concept I'd never actually considered.
2. I should be ashamed of the execution, and there's no shame like public shame.
3. In case someone from NetApp actually wants to see it. For laughs. Or whatever.
4. It's not like my "rep" can get any worse, right? .... right?

## Requirements

- Python 2 (I should port it to 3 as well, but I'm so very tired)
- NumPy
- A good sense of humour

## Usage
```
$ nate.py
```

## Credits

First of, my apologies to anyone actually reading (or, gods help you, executing) this dreck.

Second, the Python documentation project.

Third, NumPy. I hate you, but you are useful. I didn't say it was rational.

Last, but not least, Batman. I don't need a reason.
